# Proyecto de automatización con uft
POC de automatización del aplicativo ascelx.
## tecnologías
1. Uft one
2. Visual Basic Script
3. Extend report
4. git
5. Excel
## estructura del proyecto
* **Data**: contiene data de automatización
* **Evidencia**: Contiene screenShot de la ejecución
* **Reporte**: Contiene reporte personalizado
* **Resources**: Contiene las librerias y clases que usa los test cases
* **TestCase**: Contiene los casos de prueba
* **TestRunner**: Se encarga de ejecutar todos los casos según el flag que se manda en el excel

* **Runner.vbs**: Permite ejecutar los casos de prueba sin abrir UFT.

## configuración de reporte
1. usar el extend report en dll que se encuentra en la carpeta Resources/lib report
2. obtener la ruta de net frameword 4.X
3. abrir cmd como administrador
4. ejecutar el comando 
~~~bash
C:\Windows\Microsoft.NET\Framework\v4.0.30319\Regasm.exe "C:\Users\jhuamall\Documents\Unified Functional Testing\GUITestShoppingSolution\Resources\report\UFT_ExtentReports_V2\ExtentReports_UFT.dll" /codebase

~~~

## ejecutar los casos
* marcar la columna execute como "YES" de los escenarios y casos en el excel.
* Ejecutar el runner.vbs

## License
poc everis