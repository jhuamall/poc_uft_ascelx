﻿Dim x : x = Instr(Environment.Value("TestDir"),"TestRunner")-2 
Dim cstPath : cstPath = mid(Environment.Value("TestDir"),1,x) & "\"

evidencePath = cstPath + "Reporte\ReportePrincipal\evidencia\"
classPath = cstPath + "Resources\Clases\"
LoadFunctionLibrary classPath + "LoginPage.vbs"

Set logi=login()
mensaje = ""
'***********************************
'load parameter
'LoadParameters
'usuario = Parameter("viUsuario")
'password = Parameter("viContrasena")
'******************************
On error resume next
testSteps = getTestStepNameCollect()
keywords = getkeywordsCollect()
dData = getTestData()
For i = 0 To UBound(keywords) Step 1
	keyword = keywords(i)
	stepName = testSteps(i)
	data = dData(i)
	print "Test Step: " + stepName

	Select Case keyword
			Case "launchApp"
				launchApplication("iexplore")
				createTestStep stepName		
			Case "enterUserName"
				mensaje = logi.typeUserName(data)
				createTestStep stepName
				
			Case "enterPassword"
				mensaje = logi.typePassword(data)
				createTestStep stepName
				
			Case "pressLogin"
				mensaje = logi.clickOnLogin()
				createTestStepEvidence stepName, evidencePath + "login.png"
				
			Case "home"
				mensaje = logi.verifyHome()
				createTestStepEvidence stepName, evidencePath + "home.png"
			Case "closeApp"
			closeBrowser()
			createTestStep stepName
			Case else
			print "no se encontro keyword "& keyword &" para ejecutar funcion"
			createTestStep "no se encontro keyword "& keyword &" para ejecutar funcion"			
	     end Select	
Next
'validar si existe error
	If Err Then
		CheckResult "Error en Login: " & Err.Description 
		databaseSaveDatabase
		KillProcess("Excel")
		ExitTest
	End If
	
	On Error GoTo 0
CheckResult mensaje
