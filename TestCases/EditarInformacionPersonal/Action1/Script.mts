﻿Dim x : x = Instr(Environment.Value("TestDir"),"TestRunner")-2 
Dim cstPath : cstPath = mid(Environment.Value("TestDir"),1,x) & "\"

evidencePath = cstPath + "Reporte\ReportePrincipal\evidencia\"
classPath = cstPath + "Resources\Clases\"
LoadFunctionLibrary classPath + "LoginPage.vbs", classPath +"PersonalInformationPage.vbs"
'crear instancia de clases
Set logi = login()
Set perInfo = personalInformation()
result = ""

On Error Resume Next
testSteps = getTestStepNameCollect()
keywords = getkeywordsCollect()
tdata = gestTestData()

For i = 0 To UBound(keywords) Step 1
	keyword = keywords(i)
	stepName = testSteps(i)
	data = tdata(i)
	print "Test Step: " + stepName
Select Case keyword
	Case "launchApp"
		openAPP "iexplore", data
		createTestStep stepName		
	Case "enterUserName"
		mensaje = logi.typeUserName(data)
		createTestStep stepName
				
	Case "enterPassword"
		mensaje = logi.typePassword(data)
		createTestStep stepName
				
	Case "pressLogin"
		mensaje = logi.clickOnLogin()
		createTestStepEvidence stepName, evidencePath + "login.png"
				
	Case "home"
		mensaje = logi.verifyHome()
		createTestStepEvidence stepName, evidencePath + "home.png"
	Case "goToPersonalInformation"
		result = perInfo.goToPersonalInformation()
		createTestStepEvidence stepName, evidencePath + "goToPersonalInformation.png"
	Case "clickEditButton"
		result= perInfo.clickEditButton
		createTestStepEvidence stepName, evidencePath + "clickEditButton.png"
	Case "editDriverLicence"
		result = perInfo.editDriverLicence(data)
		createTestStepEvidence stepName, evidencePath + "editDriverLicence.png"
	Case "clickSaveButton"
		result = perInfo.clickSaveButton
		createTestStepEvidence stepName, evidencePath + "clickSaveButton.png"
	Case "VerifyEditMessage"
		result = "Información editado"
		createTestStepEvidence stepName, evidencePath + "clickSaveButton.png"
	Case "closeApp"
		closeBrowser()
		createTestStep stepName
	Case else
		print "no se encontro keyword "& keyword &" para ejecutar funcion"
		createTestStep "no se encontro keyword "& keyword &" para ejecutar funcion"	
End Select
Next

CheckResult result


