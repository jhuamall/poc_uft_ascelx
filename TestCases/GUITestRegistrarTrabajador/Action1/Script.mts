﻿Dim x : x = Instr(Environment.Value("TestDir"),"TestRunner")-2 
Dim cstPath : cstPath = mid(Environment.Value("TestDir"),1,x) & "\"
libPath = cstPath + "Resources\" 
excelPath = cstPath + "Data\empleados.xlsx"
LoadFunctionLibrary libPath + "Clases\RegisterPage.vbs", libPath + "Clases\LoginPage.vbs", libPath + "Function_library\ExcelUtil.vbs"

Set logi = login()
Set reg = register()
mensaje = ""

mensaje = logi.typeUserName(usuario)
mensaje = logi.typePassword(password)
mensaje = logi.clickOnLogin()

Public objExcel, objWorkbook, objWorksheet, i, RowsCount

Set objExcel = CreateObject("Excel.Application")
objExcel.Visible = False
Set objWorkbook =  objExcel.Workbooks.Open(excelPath)
Set objWorksheet = objWorkbook.Worksheets(1)

RowsCount = objWorksheet.UsedRange.Rows.Count

For i = 2 To RowsCount Step 1
	print "fila " & i
	empName = objWorksheet.Cells(i,1)
	supName = objWorksheet.Cells(i,2)
	mensaje = reg.createEmployer(empName, supName)
	print mensaje
	
	If mensaje="encontrado" Then
		objWorksheet.Cells(i,3)= "Passed"
	Else 
		objWorksheet.Cells(i,3)= "Failed"
	
	End If
Next
objWorkbook.Save
objWorkbook.Close
objExcel.Quit

Set objWorksheet = Nothing
Set objWorkbook = Nothing
Set objExcel = Nothing	

SystemUtil.Run "C:\Program Files (x86)\Micro Focus\Unified Functional Testing\samples\Flights Application\FlightsGUI.exe" 
WpfWindow("Micro Focus MyFlight Sample").WpfEdit("agentName").Set "john" @@ hightlight id_;_2024148576_;_script infofile_;_ZIP::ssf10.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfEdit("password").SetSecure "5f469b2a24b02e92c871" @@ hightlight id_;_2024150112_;_script infofile_;_ZIP::ssf11.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfButton("OK").Click @@ hightlight id_;_2024152320_;_script infofile_;_ZIP::ssf12.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfButton("FIND FLIGHTS").Click @@ hightlight id_;_2024151504_;_script infofile_;_ZIP::ssf13.xml_;_
WpfWindow("Micro Focus MyFlight Sample").Close @@ hightlight id_;_3543260_;_script infofile_;_ZIP::ssf14.xml_;_

'checkValue mensaje
'EfaTakeEvidence "Evidencia"

