﻿Print "Start Test"
Set uft = CreateObject("QuickTest.Application")
set objRepo= uft.Test.Actions(1).ObjectRepositories
const  basePath = "C:\Users\jhuamall\Documents\Unified Functional Testing\GUITestShoppingSolution\"
Dim scenarioRowCount, scenarioRun, scenarioId, scenarioId2,testCaseId, i,t
Dim stepStatus
'set excel sheet from external file
DataTable.AddSheet "Scenario"
DataTable.AddSheet "TestCase"
DataTable.AddSheet "TestStep"

'import excel sheet from external file
DataTable.ImportSheet basePath + "Data\automation.xlsx", 1,3
DataTable.ImportSheet basePath + "Data\automation.xlsx", 2,4
DataTable.ImportSheet basePath + "Data\automation.xlsx", 3,5

pathClass = basePath + "Resources\Clases\"
repo = basePath + "Resources\Object_repository\Login.tsr"
repUtilPath = basePath + "Resources\Function_library\"
LoadFunctionLibrary pathClass + "ApplicationUtils.vbs", pathClass + "LoginPage.vbs",pathClass + "RegisterPage.vbs" , repUtilPath + "ReportUtil.vbs"
evidencia = basePath + "Reporte\orange.png"
'cargar los objeto de elementos
objRepo.Add(repo)
'crear objecto
Set logi = login()
Set search = register()
mensaje = ""
'********************************************************************
' configurar reporte
createHtmlReport("Extent.html")
setEnvironment()
setTester("Raúl Huamán")
setTag("sanity, regresión")
'*********************************************************************
'read scenario

Public Function getRowCount(excelSheet)
	getRowCount = DataTable.GetSheet(excelSheet).GetRowCount
End Function



'obtener los test scenarios
Public Function getTestScenario(excelSheet)
	scenarioRowCount = getRowCount(excelSheet)
	For i = 1 To scenarioRowCount Step 1
		DataTable.SetCurrentRow(i)
		scenarioRun = DataTable(3,3)
	If UCase(scenarioRun)="YES" Then
		scenarioId = DataTable(1,3)
		testCaseId = getTestCase("TestCase")
	End If	
	Next
	
End Function

Public Function getTestCase(excelSheet)
		testCaseCount = getRowCount(excelSheet)
	For t = 1 To testCaseCount Step 1
		DataTable.SetCurrentRow(t)
		scenarioId2 = DataTable(1,excelSheet)
			testCaseRun = DataTable(3,4)
	If UCase(testCaseRun) = "YES" and scenarioId = scenarioId2 Then
		'create test case for report
		createTest DataTable(4,4)
		testCaseId = DataTable(2,4)
		getTestCase = testCaseId
		getTestStep("TestStep")
	End If	
	Next
End Function

Public Function getTestStep(excelSheet)
	On Error Resume Next
testStepCount = getRowCount(excelSheet)
For s = 1 To testStepCount Step 1
	DataTable.SetCurrentRow(s)
	testCaseId2 = DataTable(1,5)
	If testCaseId = testCaseId2 Then
		keyword = DataTable(4,5)
		'keywork steps run funcction
		Select Case keyword
			Case "launchApp"
				launchApplication("iexplore")
				createTestStepEvidence DataTable(3,5), evidencia
			Case "login"
				mensaje = logi.loginAs("Admin", "admin123")
				createTestStepEvidence DataTable(3,5), evidencia				
			Case "enterUserName"
				mensaje = logi.typeUserName(DataTable(6,5))
				createTestStepEvidence DataTable(3,5), evidencia
			Case "enterPassword"
				mensaje = logi.typePassword(DataTable(6,5))
				createTestStepEvidence DataTable(3,5), evidencia
			Case "pressLogin"
				
				mensaje = logi.clickOnLogin()
				createTestStepEvidence DataTable(3,5), evidencia
				print Err.Number &"d"& Err.Description
			Case "home"
				mensaje = logi.verifyHome()
				createTestStepEvidence DataTable(3,5), "home.png"
			Case "goToSearchEmploye"
				search.goToSearchEmpploye()
				createTestStepEvidence DataTable(3,5), evidencia
			Case "enterEmployeName"
				search.enterEmployeName("Robert")
				createTestStepEvidence DataTable(3,5), evidencia
			Case "clickOnSearchButton"
				search.clickOnSearchButton	
				createTestStepEvidence DataTable(3,5), "clickOnSearchButton.png"				
			Case "closeApp"
				closeBrowser()
				createTestStepEvidence DataTable(3,5), evidencia				
		End Select				
			DataTable.Value("Result","TestStep")= "Passed"	
	End If
Next				
End Function

getTestScenario("Scenario")

generateReport()


Print "End Test"
