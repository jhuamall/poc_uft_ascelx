﻿'=====================================================
'Test case: buscar empleado
'==================================================

Dim x : x = Instr(Environment.Value("TestDir"),"TestRunner")-2 
Dim cstPath : cstPath = mid(Environment.Value("TestDir"),1,x) & "\"

evidencePath = cstPath + "Reporte\ReportePrincipal\evidencia\"
classPath = cstPath + "Resources\Clases\"
LoadFunctionLibrary classPath + "LoginPage.vbs", classPath +"RegisterPage.vbs"
'crear objecto
Set logi = login()
Set search = register()
result = ""

'LoadParameters
usuario = Parameter("viUsuario")
password = Parameter("viContrasena")
print "parametro"+usuario
On Error Resume Next
testSteps = getTestStepNameCollect()
keywords = getkeywordsCollect()

For i = 0 To UBound(keywords) Step 1

	keyword = keywords(i)
	stepName = testSteps(i)
	print "Test Step: " + stepName
Select Case keyword
	Case "launchApp"
		launchApplication("iexplore")
		createTestStep stepName
	Case "login"
		result = logi.loginAs("Admin", "admin123")
		createTestStepEvidence stepName, evidencePath + "home.png"
	Case "goToSearchEmploye"
		search.goToSearchEmpploye()
		createTestStepEvidence stepName, evidencePath + "goToSearchEmpploye.png"
	Case "enterEmployeName"
		search.enterEmployeName("Robert")
			createTestStepEvidence stepName, evidencePath + "enterEmployeName.png"
	Case "clickOnSearchButton"
		search.clickOnSearchButton
		createTestStepEvidence stepName, evidencePath + "clickOnSearchButton.png"
	Case "verifyEmploye"
		result = "Empleado encontrado"
		createTestStepEvidence stepName, evidencePath + "clickOnSearchButton.png"
	Case "closeApp"
		closeBrowser()
		createTestStep stepName
	Case else
		print "no se encontro keyword "& keyword &" para ejecutar funcion"
		createTestStep "no se encontro keyword "& keyword &" para ejecutar funcion"		
End Select
Next

CheckResult result
