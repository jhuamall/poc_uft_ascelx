
'===============================================================
'Runner.vbs, permite ejecutar las pruebas de uft sin abrir el aplicativo
'Runner.vbs, ejecuta el test runner y exporta resultados como html
'===============================================================
'Configurar Nombre del Test Case runner y Tags del Test
public testRuner:testRuner = "TestRunner"
public testTag:testTag = "Funcional"
cstPath = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName) + "\"

'Create Uft object
Set uft = CreateObject("QuickTest.Application")

If NOT uft.Launched Then
    uft.Launch
    
End if
uft.Visible = False
uft.WindowState = "Minimized"

uft.Options.Run.ImageCaptureForTestResults = "OnError" 'OnError, Never, Always

uft.Options.Run.RunMode = "Fast" 'Normal, Fast
uft.Options.Run.ViewResults = True 
uft.Options.Run.ReportFormat = "HTML" 'HTML  

'Open uft Test
uft.Open cstPath + testRuner, False
  
'Set Result location
Set uftResultsOpt = CreateObject("QuickTest.RunResultsOptions")
uftResultsOpt.ResultsLocation = cstPath + "Reporte\ReporteUFT" 'Set the results location
'Establecer variable del tag
uft.Test.Environment.Value("tag")=testTag
'Run uft test
uft.Test.Run uftResultsOpt, True
  
'###Close uft###
uft.Test.Close
'uft.Quit
'Set uft = Nothing