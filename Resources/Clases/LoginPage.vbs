Public Function login()
	Set obj = New LoginPage
	Set login = obj
End Function


Class LoginPage
	private msj
	function typeUserName(userName)
		Browser("name:=OrangeHRM").Page("title:=OrangeHRM").Sync
		if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebEdit("html id:=txtUsername").Exist Then
			Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebEdit("html id:=txtUsername").Set userName
			takeScreenShot("username")
			msj = "ingreso el usuario"
			
			else
				msj = "element not found"
				takeScreenShot("username")
				elementNotFound()
		end if
		typeUserName = msj
	end function

	function typePassword(password)
		if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebEdit("html id:=txtPassword").Exist Then
			Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebEdit("html id:=txtPassword").SetSecure password
			msj = "ingreso el password"
			else
			msj = "element not found"
			elementNotFound()
		end if
		typePassword = msj
	end function

	function clickOnLogin()
		if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebButton("html id:=btnLogin").Exist Then
			Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebButton("html id:=btnLogin").Click
			takeScreenShot("login")
			msj = "presino login"
			else
			msj = "Element not found"
			takeScreenShot("login")
			elementNotFound()
		end if
		clickOnLogin = msj	
	end function

	function verifyHome()
		Wait(5)
		Browser("name:=OrangeHRM").Page("title:=OrangeHRM").Sync
		if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").Link("html id:=welcome").Exist Then
			msj = "Login éxitoso"
			takeScreenShot("home")
		else
			msj = "Login no éxitoso"
			takeScreenShot("home")
			elementNotFound()
		end if
		verifyHome = msj
	end function

	Function loginAs(userName, password)
		msj = typeUserName(userName)
		msj = typePassword(password)
		msj = clickOnLogin()
		msj = verifyHome()
	
		loginAs= msj
	End Function
	
End Class
