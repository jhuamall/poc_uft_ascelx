'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
'class para cargar las transacciones desde un excel file
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='

Set buscar = register()

transactionRow =2
result = ""
databaseSetDatabase "Excel", cstPath & "Data\testData.xlsx", "Master"
public function loadTransactionOneByOne()
    While hasTransaction(transactionRow)
        employeName = databaseGetValue(transactionRow, 1)
        buscar.enterEmployeName(employeName)
        buscar.clickOnSearchButton()
        result = "Transacción Registrado"
        'verificar si se registro la transaccion y marcar en excel como Transacción Registrado
        if result = "Transacción Registrado" Then
            print "Se registro la transacción"
            databaseWriteOutput(transactionRow,5,result)
        end if
        transactionRow = transactionRow + 1
    Wend
end function


'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
'verificar si existe registro de transacciones en el archivo de excel
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
Public Function hasTransaction(row)
	If databaseGetValue(row, 1) <> "" Then
		hasTransaction = true
	else
		hasTransaction = False
	End If
End Function 