'====================================================================
'class para editar los datos de informacion personal
'===================================================================
public function personalInformation()
    set obj = new PersonalInformationPage
    set personalInformation = obj
end function

class PersonalInformationPage
    'declarar object element
    private result

    function goToPersonalInformation()
        
        if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").Link("html id:=menu_pim_viewMyDetails").Exist Then
            Browser("name:=OrangeHRM").Page("title:=OrangeHRM").Link("html id:=menu_pim_viewMyDetails").Click
            result = "Pantalla de información"
            takeScreenShot("goToPersonalInformation")
        else
            elementNotFound()
            result = "element not found"
            takeScreenShot("goToPersonalInformation")
        end if
    end function

    function clickEditButton()
        if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebButton("html id:=btnSave").Exist Then
            Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebButton("html id:=btnSave").Click
            result = "Click en editar"
            takeScreenShot("clickEditButton")
        else
            elementNotFound()
            takeScreenShot("clickEditButton")
            result = "element not found"
        end if
    end function

    function editDriverLicence(driverLicence)
        if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebEdit("html id:=personal_txtLicenNo").Exist Then
            Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebEdit("html id:=personal_txtLicenNo").Set driverLicence
            result = "Información editado"
            takeScreenShot("editDriverLicence")
        else
            elementNotFound()
            takeScreenShot("editDriverLicence")
            result = "element not found"
        end if
    end function

    function clickSaveButton()
        if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebButton("html id:=btnSave").Exist Then
            Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebButton("html id:=btnSave").Click
            result = "Información editado"
            takeScreenShot("clickSaveButton")
        else
            elementNotFound()
            takeScreenShot("clickSaveButton")
            result = "element not found"
        end if
    end function

end class