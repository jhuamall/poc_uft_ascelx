Dim basePath
Dim application_url
'application_url ="https://opensource-demo.orangehrmlive.com/" 'environment.value("URL")
'=================================================
'procedimiento para configurar los navegadores
'=================================================
Public Sub launchApplication(browserName)
	If (browserName="chrome") Then
		SystemUtil.Run "chrome.exe", application_url
		
		ElseIf (browserName="iexplore") Then
		SystemUtil.Run "iexplore.exe", application_url
		
		ElseIf (browserName="edge") Then
		SystemUtil.Run "msedge.exe", application_url
		
		Else  
		print "no se encontro browser"
	End If
	wait(5)
End Sub

public sub openAPP(browserName, app)
	If (browserName="chrome") Then
		SystemUtil.Run "chrome.exe", app
		
		ElseIf (browserName="iexplore") Then
		SystemUtil.Run "iexplore.exe", app
		
		ElseIf (browserName="edge") Then
		SystemUtil.Run "msedge.exe", app
		
		Else  
		print "no se encontro browser"
	End If
	wait(5)
end Sub
'=================================================
'procedimiento para setear la url de la aplicación WEB
'=================================================
public sub setApplicationURL(url)
	application_url = url
end sub
'=================================================
'procedimiento para cerrar el navegador
'=================================================
Public Sub closeBrowser()
   Browser("name:=OrangeHRM").Quit
End Sub

public sub closeBrowsers()
	SystemUtil.CloseProcessByName "iexplore.exe"
end sub
Public Sub scrollDown()
Browser("Advantage Shopping").Page("Advantage Shopping").Highlight
Set objShell=CreateObject("WScript.Shell")
objShell.SendKeys "{PGUP}"
objShell.SendKeys "{PGDN}"
objShell.SendKeys "{END}"
End Sub
'=================================================
'procedimiento para setear la ruta base
'=================================================
public sub setBasePath(path)
	basePath = path
End sub
'=================================================
'procedimiento para mostrar mensaje de error personalizado
'cuando no se encuetra object element en tiempo de ejecución
'=================================================
public sub elementNotFound()
	Err.Raise -2147220990 ,"El elemento no fue encontrado, verifica", "no se puede localizar el object element", "error ference"
end sub

public sub takeScreenShot2(browser, object, title, success_desc, fail_desc)
   
   If object.Exist Then
   	browser.CaptureBitmap "evidencia.png", true
    Reporter.ReportEvent micPass, title, success_desc, "evidencia.png"
    
    Else 
        browser.CaptureBitmap "evidencia.png", true
        Reporter.ReportEvent micPass, title, fail_desc, "evidencia.png"
    
   End If
    
end sub
'=================================================
'procedimiento para guardar screenShot para el reporte
'=================================================
public sub takeScreenShot(screenName)
	Browser("name:=OrangeHRM").CaptureBitmap basePath +"Reporte\ReportePrincipal\evidencia\"+screenName+".png", true
end sub



