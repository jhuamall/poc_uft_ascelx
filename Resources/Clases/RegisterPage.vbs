public function register()
    set obj = New RegisterPage
    set register = obj
end function

class RegisterPage
    private msj
    

    function createEmployer(employeeName , supervisorName)
    if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").Link("innertext:=PIM").Exist(1) Then
        Browser("name:=OrangeHRM").Page("title:=OrangeHRM").Link("innertext:=PIM").Click
        msj = "ok"
        if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebButton("value:=Search").Exist Then
            Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebEdit("html id:=empsearch_employee_name_empName").Set employeeName
            Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebEdit("html id:=empsearch_supervisor_name").Set supervisorName
            Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebButton("value:=Search").Click
            
            Browser("name:=OrangeHRM").CaptureBitmap "evidencia.png", true
            Reporter.ReportEvent micPass, "busqueda de trabajador", "busqueda exitoso", "evidencia.png" 
            msj = "encontrado"
            Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebButton("name:=Reset").Click
            msj = "encontrado"
        else
            msj= "element not found"
        end if
    else
        msj = "element not found"
    end if
    createEmployer = msj
    end function
'============================================
'Create function step by step to search employes
'===============================================

function goToSearchEmpploye()
    if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").Link("innertext:=PIM").Exist(1) Then
        Browser("name:=OrangeHRM").Page("title:=OrangeHRM").Link("innertext:=PIM").Click
        takeScreenShot("goToSearchEmpploye")
    else
        elementNotFound()
    end if

end function

function enterEmployeName(empName)
    if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebEdit("html id:=empsearch_employee_name_empName").Exist Then
        Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebEdit("html id:=empsearch_employee_name_empName").Set empName
        takeScreenShot("enterEmployeName")
    end if
end function

function clickOnSearchButton()
    if Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebButton("value:=Search").Exist Then
        Browser("name:=OrangeHRM").Page("title:=OrangeHRM").WebButton("value:=Search").Click
        takeScreenShot("clickOnSearchButton")        
    end if
end function
end class