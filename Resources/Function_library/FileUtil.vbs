Public path
path = "C:\Users\jhuamall\Documents\Unified Functional Testing\GUITestShoppingSolution\Data\"
Public Function createFile(fileName)
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set file = fso.CreateTextFile(path & fileName,true)
	file.Write("test automation demo for uft")
	file.Close
	Set fso = Nothing
	Set file = Nothing
End Function

' this method read value from text file

Public Function readTextFile(fileName)
    
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set file = fso.OpenTextFile(path & fileName)
	
	Do  until file.AtEndOfStream
	strText = file.ReadLine
	print strText
	
	Loop 
	
End Function

Public Function deleteFile(FileName)
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	If fso.FileExists(path & FileName) = true Then
		fso.DeleteFile(path & FileName)
		
		Else 
		MsgBox "file not founded", vbError
	End If
	
End Function
