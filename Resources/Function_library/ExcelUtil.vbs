Public objExcel, objWorkbook, objWorksheet, i, RowsCount
Public excelPath
' this method set the value into excel cell
Public Function setValue(row, cell, value)
	objWorksheet.Cells(row,cell)= value
End Function
' this method get the value from excel cell	
Public Function getTotalRows(excel)
	Set objExcel = CreateObject("Excel.Application")
	objExcel.Visible = False
	Set objWorkbook =  objExcel.Workbooks.Open(excel)
	Set objWorksheet = objWorkbook.Worksheets(1)
	RowsCount = objWorksheet.UsedRange.Rows.Count
	getTotalRows = RowsCount
End Function

'this method compare the expected and actual value into excel and mark
Public Function testMark(excel,result)
Set objExcel = CreateObject("Excel.Application")
objExcel.Visible = False
Set objWorkbook =  objExcel.Workbooks.Open(excel)
Set objWorksheet = objWorkbook.Worksheets(1)

RowsCount = objWorksheet.UsedRange.Rows.Count

For i = 2 To RowsCount Step 1
	Expected = objWorksheet.Cells(i,1)
	Actual = objWorksheet.Cells(i,2)
	
	If result="encontrado" Then
		objWorksheet.Cells(i,3)= "Passed"
	Else 
		objWorksheet.Cells(i,3)= "Failed"
	
	End If
Next
objWorkbook.Save
objWorkbook.Close
objExcel.Quit

Set objWorksheet = Nothing
Set objWorkbook = Nothing
Set objExcel = Nothing	
	
End Function

