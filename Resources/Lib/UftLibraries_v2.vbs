
'===== Window Variables =====
Public UFTApplication, UFTApplicationName, UFTWindow, UFTWindowName
'===== Class Variables =====
Public UFTTest, SAPTest
'===== Test Variables =====
Public onError, evidenceDepth, UFTTries, UFTwait, stringOnError, baseApplication
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' startSever - initialize variables 
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub startSever
	Set UFTTest = New Server  'Initiates the Server Class
	setOnError(False) 'Resets the onError variable
	setTries(1) 'sets the number of tries to do before failing the test
	setWait(1) 'sets the wait between tries (in seconds)
	setDepth(1) 'sets the evidence depth, 0 no evidence, 1 evidence after step, 2 evidence before and after
	setStep(1) 'initiates the step numerator
	setScene(1) 'initiates the scene numerator
	setActionList 'Creates the list objects
	stringOnError = "" 'Variable that receives the error line
End Sub


'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' Lectura de parametros
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function PropertyParameters (names)
		Dim namesAtt, attributes
	
		namesAtt = Split (names, ",")
		If (UBound(namesAtt)+1 = 1) Then
			attributes = namesAtt(0)
		Else
			For j = 0 to UBound(namesAtt)
				If (j = 0) Then
					attributes = namesAtt(j)
				Else
					attributes = attributes & """" & "," & """" & namesAtt(j)	
				End If
			Next				
		End If
		PropertyParameters = attributes
End Function

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' serverSetApplicationBase1 - Set aplication base  
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub serverSetApplicationBase1(app1, name1)
	baseApplication = app1 & "(""" & name1 & """)"
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' serverSetApplicationBase1 - Set aplication base  
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub serverSetApplicationBase2(app1, name1, app2, name2)
	baseApplication = app1 & "(""" & name1 & """)" & "." & app2 & "(""" & name2 & """)"
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' serverSetApplicationBase1 - Set aplication base  
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub serverSetApplicationBase3(app1, name1, app2, name2, app3, name3)
	baseApplication = app1 & "(""" & name1 & """)" & "." & app2 & "(""" & name2 & """)" & "." & app3 & "(""" & name3 & """)"
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' serverSetApplicationBase1 - Set aplication base  
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub serverSetApplicationBase4(app1, name1, app2, name2, app3, name3, app4, name4)
	baseApplication = app1 & "(""" & name1 & """)" & "." & app2 & "(""" & name2 & """)" & "." & app3 & "(""" & name3 & """)" & "." & app4 & "(""" & name4 & """)"
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' serverSetApplicationBase1 - Set aplication base  
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub serverSetApplicationBase5(app1, name1, app2, name2, app3, name3, app4, name4, app5, name5)
	baseApplication = app1 & "(""" & name1 & """)" & "." & app2 & "(""" & name2 & """)" & "." & app3 & "(""" & name3 & """)" & "." & app4 & "(""" & name4 & """)" & "." & app5 & "(""" & name5 & """)"
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' serverSetApplicationBase1 - Set aplication base  
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub serverSetApplicationBase6(app1, name1, app2, name2, app3, name3, app4, name4, app5, name5, app6, name6)
	baseApplication = app1 & "(""" & name1 & """)" & "." & app2 & "(""" & name2 & """)" & "." & app3 & "(""" & name3 & """)" & "." & app4 & "(""" & name4 & """)" & "." & app5 & "(""" & name5 & """)" & "." & app6 & "(""" & name6 & """)"
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' serverSetApplicationBase1 - Set aplication base  
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub serverSetApplicationBase7(app1, name1, app2, name2, app3, name3, app4, name4, app5, name5, app6, name6, app7, name7)
	baseApplication = app1 & "(""" & name1 & """)" & "." & app2 & "(""" & name2 & """)" & "." & app3 & "(""" & name3 & """)" & "." & app4 & "(""" & name4 & """)" & "." & app5 & "(""" & name5 & """)" & "." & app6 & "(""" & name6 & """)" & "." & app7 & "(""" & name7 & """)"
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' setOnError - Sets the onError variable
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub setOnError(value)
	onError = value
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' setTries - sets the number of tries to do before failing the test
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub setTries(tries)
	UFTTries = tries
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' setWait - sets the wait between tries (in seconds)
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub setWait(seconds)
	UFTwait = seconds
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' setDepth - sets the evidence depth, 0 no evidence, 1 evidence after step, 2 evidence before and after
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub setDepth(depth)
	evidenceDepth = depth
End Sub



'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
'Class Server - class that couples instructions of commands and logs
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//

Class Server
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Verifying Method: waitElement -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function waitElement(locator)
		Dim exists 'Initiates the variable to check if exists
		exists = false	'Initiates the variable to false
		For n = 1 To UFTTries 'loop to try to find the object
			Execute("exists = " & locator & ".Exist") 'Executes Eval to find the objects
			If exists = true Then 'Fork if the object has been found
				waitElement = True 'sets the variable to indicate it has been found
				Exit Function 'returns
			End If
			wait(UFTwait) 'wait before doing another try
		Next
		waitElement = False 'sets the function to false
	End Function



	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: Sett -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function Sett(window, param, value)
	If onError Then 
		efaLogTest 4, "Set", param, value, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("Set")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".Set """ & value & """")
			efaLogTest 10, "Set", param, value, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "Set", param, value, ""
	
	End Function
		'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: SendKey -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function SendKey(window, param, value)
	If onError Then 
		efaLogTest 4, "SendKey", param, value, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("SendKey")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".SendKey " & value)
			efaLogTest 10, "SendKey", param, value, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "SendKey", param, value, ""
	End Function	
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: Sync -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function Sync(window, param, value)
	If onError Then 
		efaLogTest 4, "Sync", param, value, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("Sync")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".Sync """ & value & """")
			efaLogTest 10, "Sync", param, value, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "Sync", param, value, ""
	End Function	
	
	
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: ActivateCell -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function ActivateCell(window, param, value1, value2)
	If onError Then 
		efaLogTest 4, "ActivateCell", param, value1 & "|" & value2, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("ActivateCell")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".ActivateCell " & value1 & """" & value2 & """")
			efaLogTest 10, "ActivateCell", param, value1 & "|" & value2, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "ActivateCell", param, value1 & "|" & value2, ""
	End Function	
	

	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: Click -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function Click(window, param)
	If onError Then 
		efaLogTest 4, "Click", param, "", ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("Click")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".Click")
			efaLogTest 10, "Click", param, "", TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "Click", param, "", ""
	End Function	
	
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: ClickCell -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function ClickCell(window, param, value1, value2)
	If onError Then 
		efaLogTest 4, "ClickCell", param, value1 & "|" & value2, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("ClickCell")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".ClickCell " & value1 & ", """ & value2 & """")
			efaLogTest 10, "ClickCell", param, value1 & "|" & value2, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "ClickCell", param, value1 & "|" & value2, ""
	End Function
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: SelectCell -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function SelectCell(window, param, value1, value2)
	If onError Then 
		efaLogTest 4, "SelectCell", param, value1 & "|" & value2, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("SelectCell")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".SelectCell " & value1 & ", """ & value2 & """")
			efaLogTest 10, "SelectCell", param, value1 & "|" & value2, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "SelectCell", param, value1 & "|" & value2, ""
	End Function
	
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: GetCellData -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function GetROProperty(window, param, value)
	If onError Then 
		efaLogTest 4, "GetROProperty", param, value, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("GetROProperty")
		If waitElement(base) Then
			TimeBegin
			Execute("GetROProperty = " & base & ".GetROProperty(""" & value & """)")
			efaLogTest 10, "GetROProperty", param, value, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		GetROProperty = "Não foi possível executar a ação"
		efaLogTest 10, "GetROProperty", param, value, ""
	End Function
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: GetCellData -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function GetCellData(window, param, value1, value2)
	If onError Then 
		efaLogTest 4, "GetCellData", param, value1 & "|" & value2, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("GetCellData")
		If waitElement(base) Then
			TimeBegin
			Execute("GetCellData = " & base & ".GetCellData(" & value1 & ", """ & value2 & """)")
			efaLogTest 10, "GetCellData", param, value1 & "|" & value2, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		GetCellData = "Não foi possível executar a ação"
		efaLogTest 10, "GetCellData", param, value1 & "|" & value2, ""
	End Function
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: SetCellData -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function SetCellData(window, param, value1, value2, value3)
	If onError Then 
		efaLogTest 4, "SetCellData", param, value1 & "|" & value2 & "|" & value3, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("SetCellData")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".SetCellData " & value1 & ", """ & value2 & """" & ", """& value3 & """")
			efaLogTest 10, "SetCellData", param, value1 & "|" & value2 & "|" & value3, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "SetCellData", param, value1 & "|" & value2 & "|" & value3, ""
	End Function

	'UFTTest.SelectRow "SAPGuiGrid", "GridViewCtrl", 1
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: Selectt -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function SelectRow(window, param, value)
	If onError Then 
		efaLogTest 4, "SelectRow", param, value, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("SelectRow")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".SelectRow """ & value & """")
			efaLogTest 10, "SelectRow", param, value, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "SelectRow", param, value, ""	
	End Function
	
	
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: Selectt -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function Selectt(window, param, value)
	If onError Then 
		efaLogTest 4, "Select", param, value, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("Select")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".Select """ & value & """")
			efaLogTest 10, "Select", param, value, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "Select", param, value, ""	
	End Function
	
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: Navigate -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function Navigate(value)
	If onError Then 
		efaLogTest 4, "Navigate", UFTApplicationName, value, ""
		Exit Function
	End If
	Dim base
		base =  UFTApplication & "(""" & UFTApplicationName & """)"
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("Navigate")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".Navigate """ & value & """")
			efaLogTest 10, "Navigate", UFTApplicationName, value, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "Navigate", UFTApplicationName, value, ""	
	End Function
	
	
		'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: SelectColumn -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function SelectColumn(window, param, value)
	If onError Then 
		efaLogTest 4, "SelectColumn", param, value, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("SelectColumn")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".SelectColumn """ & value & """")
			efaLogTest 10, "SelectColumn", param, value, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "SelectColumn", param, value, ""
	
	End Function
		'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: PressButton -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
	Public Function PressButton(window, param, value)
	If onError Then 
		efaLogTest 4, "PressButton", param, value, ""
		Exit Function
	End If
	Dim base
	If window = "self" Then
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)"
	Else
		base =  UFTApplication & "(""" & UFTApplicationName & """)." & UFTWindow & "(""" & UFTWindowName & """)." + window + "(""" & param & """)"
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence("PressButton")
		If waitElement(base) Then
			TimeBegin
			Execute(base & ".PressButton """ & value & """")
			efaLogTest 10, "PressButton", param, value, TimeEnd
			Exit Function
		Else
			setOnError(True)
		End If
		efaLogTest 10, "PressButton", param, value, ""
	
	End Function


	
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: Execute -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//	
	Public Function ExecuteAction(action, objectClass, objectProperty, parameter)
	values = Split(parameter, "|") 'splits the attributes into values
	objectProperty = PropertyParameters(objectProperty)
	If onError Then 'Checks if the application test has failed
		efaLogTest 4, action, objectProperty, parameter, "" 'blocks the step if any step before has failed
		Exit Function
	End If
	Dim base 'initiates the base variable
	Dim actualValue
	actualValue = ""
	If objectClass = "self" Then 'if the objectClass variable is set as self, base variable will be only the baseApplication
		base =  baseApplication 'sets base as baseApplication
	Else
		base =  baseApplication + "." + objectClass + "(""" & objectProperty & """)" 'includes in base variable the objectClass and objectProperty
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence(action) 'takes an evidence if the evidenceDepth is greater than 1
		If waitElement(base) Then 'Checks if object is visible in the application
			actionbase = action & " " 'sets actionbase to the action
			If Ubound(values) > -1 Then
				If values(0) <> "N/A" Then 'checks if there are attributes
					For i = 0 To Ubound(values) 'iteration with the values array
						'actionbase = actionbase '+ values(i) 'adds the attributes to the actionbase string
						actualValue = actualValue + values(i)
						If i < Ubound(values) Then actualValue = actualValue + ", " 'adds a comma if needed (extra attributes)
					Next
				End if
			End If
			
			TimeBegin 'starts the timer
			If actualValue = "" Then
				Execute(base & "." & actionbase) 'Executes the UFT action without parameters
			ElseIf action = "Click" and actualValue <> "" Then
				Execute(base & "." & actionbase & actualValue) 'Executes the UFT action
			ElseIf action = "SendKey" and actualValue <> "" Then
			Execute(base & "." & actionbase & actualValue) 'Executes the UFT action
			ElseIf action = "PressButton" and actualValue <> "" Then
			Execute(base & "." & actionbase & actualValue) 'Executes the UFT action
			Else
				Execute(base & "." & actionbase & actualValue) 'Executes the UFT action with String parameters
			End If
			efaLogTest 10, action, param, parameter, TimeEnd 'Logs the step made, and passed the execution time of the step
			Exit Function 'Returns the function
			
		Else
			setOnError(True) 'Sets the error in the caseTest
		End If
		efaLogTest 10, action, objectProperty, parameter, "" 'Logs the step made
	End Function
	
	
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//
    ' ---------- Action Method: Receive -------------
    '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=//	
	Public Function RecieveAction(action, objectClass, objectProperty, parameter)
	values = Split(parameter, "|") 'splits the attributes into values
	objectProperty = PropertyParameters(objectProperty)
	If onError Then 'Checks if the application test has failed
		efaLogTest 4, action, objectProperty, parameter, "" 'blocks the step if any step before has failed
		Exit Function
	End If
	Dim base 'initiates the base variable
	Dim actualValue
	actualValue = ""	
	If objectClass = "self" Then 'if the objectClass variable is set as self, base variable will be only the baseApplication
		base =  baseApplication 'sets base as baseApplication
	Else
		base =  baseApplication + "." + objectClass + "(""" & objectProperty & """)" 'includes in base variable the objectClass and objectProperty
	End If
	'listAdd
		If evidenceDepth > 1 Then efaTakeEvidence(action) 'takes an evidence if the evidenceDepth is greater than 1
		If waitElement(base) Then 'Checks if object is visible in the application
			actionbase = action & " " 'sets actionbase to the action
			If values(0) <> "N/A" Then 'checks if there are attributes
				For i = 0 To Ubound(values) 'iteration with the values array
					'actionbase = actionbase + values(i) 'adds the attributes to the actionbase string
					actualValue = actualValue + values(i)
					If i < Ubound(values) Then actualValue = actualValue + ", " 'adds a comma if needed (extra attributes)
				Next
			End If
			TimeBegin 'starts the timer
			
			If actualValue = "" Then
				Execute("RecieveAction = " & base & "." & actionbase) 'Executes the UFT action
			ElseIf action = "GetCellData" and actualValue <> "" Then
				Execute("RecieveAction = " & base & "." & actionbase & "(" & actualValue & ")") 'Executes the UFT action
			Else
				Execute("RecieveAction = " & base & "." & actionbase & "(" &  Chr(34) & actualValue & Chr(34)  & ")") 'Executes the UFT action
			End If
			
			
			'Execute("RecieveAction = " & base & "." & actionbase) 'Executes and returns the parameter through the Evaluation
			efaLogTest 10, action, objectProperty, parameter, TimeEnd 'Logs the step made, and passed the execution time of the step
			Exit Function 'Returns the function
		Else
			setOnError(True) 'Sets the error in the caseTest
		End If
		efaLogTest 10, action, objectProperty, parameter, "" 'Logs the step made
	End Function
End Class





