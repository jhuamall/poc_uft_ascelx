Option Explicit
Public testSceneRow,testCaseRow, testStepRow,testCaseKeyWord, stepKeyword
public scenarioName, testCaseName,testCategory, testStepName,timeRow, testData
Public scenarioId, scenarioId2
Public testCaseId, testCaseId2
dim keywordsCollect:keywordsCollect = Array()
dim testStepNameCollect:testStepNameCollect = Array()
dim testDataCollect:testDataCollect = Array()
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' hasTests - verify if exist test in excel file(.xlsx)
' row as Integer
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
Public Function hasTests(row)
	If databaseGetValue(row, 3) <> "" Then
		hasTests = true
	else
		hasTests = False
	End If
End Function 
'==================================================
' runTestScenario: obtiene y ejecuta los casos de prueba  coinciden
'con el scenario
'====================================================

Public Function runTestScenario(basePath)
    testSceneRow = 2
    'read scenarios
    While hasTests(testSceneRow)
        If UCase(databaseGetValue(testSceneRow, 3)) = "YES" Then
            scenarioName = databaseGetValue(testSceneRow, 2)
            scenarioId = databaseGetValue(testSceneRow, 1)
            databaseChangeTable("TestCase")
            testCaseRow = 2
            'read test cases
            getTestCase(basePath)
            databaseChangeTable "Scenario"
        end If
        testSceneRow = testSceneRow + 1
    Wend    
End Function

'==================================================
' getTestCase: obtiene los casos de prueba del excel
'y ejecuta los test steps
'==================================================
public function getTestCase(basePath)
    While hasTests(testCaseRow)
        scenarioId2 = databaseGetValue(testCaseRow,1)
        If UCase(databaseGetValue(testCaseRow, 3)) = "YES" and scenarioId=scenarioId2 Then
    
            testCaseId = databaseGetValue(testCaseRow, 2)
            testCaseKeyWord = databaseGetValue(testCaseRow, 4)
            testCaseName = databaseGetValue(testCaseRow, 5)
            testCategory = databaseGetValue(testCaseRow, 6)
            timeRow = testCaseRow
            'create test case name and category for report 
            createTest testCaseName
            createTestCategory testCategory
            print "Test case: " + testCaseName
            print "--------------------------------------"
            databaseChangeTable("TestStep")
            testStepRow = 2
            'reiniciar test step por caso
            If UBound(keywordsCollect) <> -1 Then
            	Erase keywordsCollect
            	keywordsCollect=Array()
            End If
            If UBound(testStepNameCollect) <> -1 Then
            	Erase testStepNameCollect
            	testStepNameCollect=Array()
            End If
            ' reiniciar testDataCollect
            If UBound(testDataCollect) <> -1 Then
            	Erase testDataCollect
            	testDataCollect=Array()
            End If
            TimeBeginTransaction
            getTestStep()
            'run test case
            LoadAndRunAction basePath & "TestCases\" & testCaseKeyWord, "Action1"
            databaseWriteOutput timeRow, 10, TimeEndTransaction
            databaseWriteOutput timeRow, 11, DateNow
            
            databaseChangeTable "TestCase"
        end if 
        testCaseRow = testCaseRow + 1
    Wend
end function

'==============================================================
' getTestStep: ejecuta los step de los test cases
'==============================================================
public function getTestStep()
    While hasTests(testStepRow)
        testCaseId2 = databaseGetValue(testStepRow, 1) 
        If testCaseId=testCaseId2 Then
            stepKeyword = databaseGetValue(testStepRow, 3)
            testStepName = databaseGetValue(testStepRow, 4)
            testData = databaseGetValue(testStepRow,5)
            'create keyword collect
            Redim Preserve keywordsCollect(UBound(keywordsCollect)+1)
            keywordsCollect(UBound(keywordsCollect)) = stepKeyword

            'create test stepName collect
            Redim Preserve testStepNameCollect(UBound(testStepNameCollect)+1)
            testStepNameCollect(UBound(testStepNameCollect)) = testStepName

            'create testData collect
            Redim Preserve testDataCollect(UBound(testDataCollect)+1)
            testDataCollect(UBound(testDataCollect)) = testData
        end if
        testStepRow = testStepRow + 1
    Wend   
end function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' getkeywordsCollect; crea un array con las keyword y retorna
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
public function getkeywordsCollect()
    getkeywordsCollect = keywordsCollect

end function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
'  getTestStepNameCollect; crea un array con los nombres de los steps y retorna
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
public function getTestStepNameCollect()
    getTestStepNameCollect = testStepNameCollect
end function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' getTestData: crear array con los test data y retorna el array
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
public function getTestData()
    getTestData = testDataCollect
end function
public function getTestCaseName()
    getTestCaseName = testCaseName
end function

public function getTestStepName()
    getTestStepName = testStepName
end function
'====================================================
'verificar el resultado esperado y obtenido
'=====================================================
Public Sub CheckResult(value)
    dim expectedResult
	databaseChangeTable "TestCase"
	expectedResult = databaseGetValue(testCaseRow, 7)
	If expectedResult = value Then
			databaseWriteOutput testCaseRow, 9, "Passed"
		else
			databaseWriteOutput testCaseRow, 9, "Failed"
	End If
    databaseWriteOutput testCaseRow, 8, value
    databaseSaveDatabase
End Sub
