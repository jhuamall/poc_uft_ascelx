'=====================================================================
' libreria para generar reporte html con extent report
' se crea métodos para armar el reporte

'=====================================================================

public htmlReporter, path, testerName, tagName

Public Sub createHtmlReport(htmlReport)
	'path = "C:\Users\jhuamall\Documents\Unified Functional Testing\GUITestShoppingSolution\Reporte\"
	'report = path & htmlReport
	Set htmlReporter = CreateObject("UFT_Extent_Reports.HTMLReporter")
    htmlReporter.InitializeReport(htmlReport)
    htmlReporter.AddReportName("TEST AUTOMATION REPORT")
    htmlReporter.AddDocumentTitle("UFT Report")
End Sub

Public Sub createTest(testName)
    htmlReporter.CreateTest(testName)
    call htmlReporter.AssignAuthorToTest(testerName)
	
End Sub

public sub createTestCategory(tag)
    call htmlReporter.AssignCategoryToTest(tag)
end sub

public sub setTester(tester)
    testerName = tester
end Sub

public Sub setTag(tag)
    tagName = tag
end Sub

Public sub setEnvironment(hostName, system,toolName, environment)
    call htmlReporter.AddSystemInfo("Host",hostName )
    call htmlReporter.AddSystemInfo("Operation System", system)
    call htmlReporter.AddSystemInfo("automation tool", toolName)
    call htmlReporter.AddSystemInfo("environment", environment)

end sub
Public Sub createTestStepEvidence(testStepName,image)
	'evidence = path & image
    'call htmlReporter.AddInfoLog(testStepName)
    If Err.Number <> 0 Then
     call htmlReporter.AddFailLog(testStepName, image)
     Call htmlReporter.AddErrorLog(Err.Number &":"& Err.Description & ":"&Err.Source)
     Reporter.ReportEvent  micFail, Err.Description, Err.Source
     print Err.Number &":"& Err.Description & ":"&Err.Source
     Err.Clear
    else
        call htmlReporter.AddPassLog(testStepName, image)
    End If
    
End Sub

Public Sub createTestStep(testStepName)
    'call htmlReporter.AddInfoLog(testStepName)
    'call htmlReporter.AddErrorLog
    If Err.Number <> 0 Then
        call htmlReporter.AddFailLog(testStepName)
        Call htmlReporter.AddErrorLog(Err.Number &":"& Err.Description & ":"&Err.Source)
        Reporter.ReportEvent  micFail, Err.Description, Err.Source
        print Err.Number &":"& Err.Description & ":"&Err.Source
        Err.Clear
       else
        call htmlReporter.AddPassLog(testStepName)
       End If
End Sub

Public Sub generateReport()
	htmlReporter.ChangeToDarkTheme
    htmlReporter.GenerateReport()
End Sub

Function getStepStatus()
	stepStatus = Reporter.RunStatus
	getStepStatus = stepStatus
End Function

	
