'*********************************************************************************************************************************
'*********************************       DATABASEMANAGEMENT                     *********************************
'*********************************************************************************************************************************
Public oExcel, oWB, oTable, oSheet, databaseExcel

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' databaseChangeTable - Change table EFA Master
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub databaseChangeTable(table)
	oTable = table
	set oSheet=oWB.WorkSheets(table)
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' setPath - Set the evidence path
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub databaseSetDatabase(database, path, sheet)
	Select Case database
		Case "Excel"
			databaseSetExcel path, sheet
			Exit Sub
	End Select			
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' databaseGetValue - Get value of table and return for script 
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Function databaseGetValue(linha,coluna)
	getVal=oSheet.Cells(linha,coluna).Value
	databaseGetValue = getVal
	' InsertLog("Valor setado com sucesso")
End Function

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' databaseWriteOutput - pass value in table for value extern
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  

Public Sub databaseWriteOutput(linha, coluna, value)
	oSheet.Cells(linha,coluna).Value=value
End Sub
	
	'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' idono
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
	
Public Sub databaseAddRow(row)
	oSheet.Cells(row, 1).EntireRow.Insert
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' databaseSetExcel - Open Table (.xlsx)
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub databaseSetExcel(path, sheet)
	Set oExcel=CreateObject("Excel.Application")
	Set oWB=oExcel.Workbooks.Open(path)
	databaseChangeTable sheet
    'Set oSheet=oWB.WorkSheets(sheet)
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' databaseSaveDatabase - Save Table with evidence and informations
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub databaseSaveDatabase()
	oWb.Save
End Sub

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' getIDOrdem - Get Id or Order of execution
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Function getIDOrdem(id, ordem)
	Dim oExcel, oWB, oSheet, linha
	Set oExcel=CreateObject("Excel.Application")
	Set oWB=oExcel.Workbooks.Open("C:\Natura\Abas.xlsx")
	Set oSheet=oWB.WorkSheets("id")
	oExcel.Visible=TRUE
	linha = 1

	While oSheet.Cells(linha,6).Value <> ""
		If oSheet.Cells(linha,6).Value = id and oSheet.Cells(linha,7).Value = ordem Then
			getIDOrdem = linha
			Exit Function
		End If	
		linha = linha + 1
	Wend
	
	Set oExcel=Nothing	
	getIDOrdem = 0
	
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' loadParameters - Load Parameters adds in UFT 
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  

Public Sub loadParameters
	col = 1
	While databaseGetValue(1, col) <> ""
		On Error Resume Next
		if Mid(databaseGetValue(testCaseRow, col), 1, 9) = "getValue(" then
			Parameter(databaseGetValue(1, col)) = getDBQuery(databaseGetValue(testCaseRow, col))
		else
			Parameter(databaseGetValue(1, col)) = databaseGetValue(testCaseRow, col)
		end if
		
		col = col + 1
	Wend
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' getDBQuery - Get value of other cell the same sheet 
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  

Public Function getDBQuery(Query)
	valueString = ""
	value = Split(Query, "(")
	valueString = Replace(value(1), ")", "")
	value = Split(valueString, ",")
	getDBQuery = databaseGetValue(value(0), getColumnByName(value(1)))
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' getColumnByName - Search the column that has the name of variable
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Function getColumnByName(name)
	col = 1
	While databaseGetValue(1, col) <> ""
		if databaseGetValue(1, col) = name then
			getColumnByName = col
			Exit Function
		End If
		col = col + 1
	Wend
	getColumnByName = col
End Function




