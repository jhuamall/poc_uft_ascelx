Public pathEvidence, testScene, testStep  
Public testCaseRow, actionList, stepList
Public applicationNameEvidence, applicationAppEvidence
Public libTimeSave, libTimeTransaction
Public caseTestName
Public oFSO
Public pathStorageEvidence, countImages, pathImages
Public arrList
	

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' setPath - Set the evidence path
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub setPath(path)

	If Not oFSO.FolderExists(path) Then
		crearArbol path
	End If
	
	pathEvidence = path
End Sub

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' crearArbol - crea un arbol de directorios
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Sub crearArbol(path)
	
	arrLista = Split(path, "\")
	directorio = arrLista(0)
	
	For Iterator = 1 To UBound(arrLista) Step 1
		
		If Not oFSO.FolderExists(directorio) Then
			oFSO.CreateFolder directorio
		End If
		
		directorio = directorio & "\" & arrLista(Iterator)
		
	Next
	
	
End Sub

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' setStep - sets the actual step number
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Sub setStep(st)
	testStep = st
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' setScene - sets the actual scene number
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Sub setScene(sc)
	testScene = sc
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' setActionList - creates the List and Folder objects
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Sub setActionList()
	Set actionList = CreateObject( "System.Collections.ArrayList" ) 'Creates the list of actions
	Set stepList = CreateObject( "System.Collections.ArrayList" ) 'Creates the list of steps
	Set oFSO = CreateObject("Scripting.FileSystemObject") 'Creates the folder navigator object
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' getScene - gets the scene number as a string
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function getScene()
	If testScene < 10 Then 'Fork to check the number and put 0s before the scene number
		getScene = "00" & testCaseRow'testScene
	ElseIf testStep < 100 Then
		getScene = "0" & testCaseRow'testScene
	Else
		getScene = testCaseRow'testScene
	End If
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' getStep - gets the step number as a string
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function getStep()
	If testStep < 10 Then 'Fork to check the number and put 0s before the step number
		getStep = "00" & testStep
	ElseIf testStep < 100 Then
		getStep = "0" & testStep
	Else
		getStep = testStep
	End If
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' getStatus - Rank status of each step
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function getStatus(statusID)
	Select Case statusID 'Quality Center IDs of each status
	Case 0 'Passed ID QC Number
		getStatus = "Passed"
	Case 1 'Failed ID QC Number
		getStatus = "Failed"
	Case 4 'Blocked ID QC Number
		getStatus = "Blocked"
	Case Else 'If it didn't find a mapped status
		getStatus = "N/A"
	End Select
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' convertToStep - Converts the step number do string format
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function convertToStep(st)
	If st < 10 Then 'Checks the actual number and adds 0s before itself
		convertToStep = "00" & st
	ElseIf st < 100 Then
		convertToStep = "0" & st
	Else
		convertToStep = st
	End If
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' -------------------- Report Functions ------------------------------------------
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  



'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
'efaLogTest - verify status and adds in log

' Report - Report Log
' status as integer
' action as string
' stepAction as string
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  
Public Sub efaLogTest(status, action, locator, value, timeExec)
	
	If status = 10 Then 'Status 10 means it still hasn't decided if it passed or failed.
		If onError = True Then  'If on error it will set the status to 1(Failed)
			status = 1 'QC status 1 - Failed
		Else
			status = 0 'QC status 0 - Passed
		End If
	End If
	addToStepLog status, action, locator, value 'Adds the step to the log
	addToStepList status, action, locator, value, timeExec 'Adds the step to the Step List
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' addToStepLog - Create log with information of step test
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function addToStepLog(status, action, locator, value)
	stepString = "" 'Initiates variable
	stepString = stepString & status & "|" 'Adds status to the variable
	stepString = stepString & locator & "|" 'Adds the locator to the variable
	stepString = stepString & value 'Adds the parameters to the variable
	stepList.Add stepString 'Adds the string to the stepList
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' addToStepList - Description of each command with step 
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function addToStepList(status, action, locator, value, timeExec)
	stepInfo = "" 'Initiates the variable
	value = Split(value, "|") 'Splits the attributes
	Select Case action 'Selects the case depending on the action
	Case "Navigate"
		stepInfo = stepInfo + "Navegar|"
		stepInfo = stepInfo + "Navegar a la página '" & value(0) & "'"
		stepInfo = stepInfo + "|La página se abre con exito"
		If status = 0 Then
			stepInfo = stepInfo + "|La página se abre con exito"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|La página no se abre correctamente"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecutada"
		End if
	
	Case "Click"
		stepInfo = stepInfo + "Clicar|"
		stepInfo = stepInfo + "Hacer clic en el elemento '" & locator & "'"
		stepInfo = stepInfo + "|El elemento se hace clic con éxito"
		If status = 0 Then
			stepInfo = stepInfo + "|El elemento se hace clic con éxito"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|El elemento no se hizo clic con éxito"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecutada"
		End if
	Case "Exist"
		stepInfo = stepInfo + "Validar|"
		stepInfo = stepInfo + "Validar si el elemento '" & locator & " existe'"
		stepInfo = stepInfo + "|El elemento se muestra correctamente"
		If status = 0 Then
			stepInfo = stepInfo + "|El elemento se muestra correctamente"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|El elemento no se muestra correctamente"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecutada"
		End if
	Case "GetROProperty"
		stepInfo = stepInfo + "GetROProperty|"
		stepInfo = stepInfo + "Obtener del campo '" & locator & "' la propiedad '" & value(0) & "'"
		stepInfo = stepInfo + "|El valor obtenido con éxito"
		If status = 0 Then
			stepInfo = stepInfo + "|El valor obtenido con éxito"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|El valor no se obtuvo con éxito"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecutada"
		End if
	Case "SetText"
		stepInfo = stepInfo + "SetText|"
		stepInfo = stepInfo + "Setear el campo '" & locator & "' con el valor '" & value(0) & "'"
		stepInfo = stepInfo + "|El campo se ha completado con éxito"
		If status = 0 Then
			stepInfo = stepInfo + "|El campo se ha completado con éxito"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|El campo no se ha completado con éxito"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecuta"
		End if
	Case "Set"
		stepInfo = stepInfo + "Set|"
		stepInfo = stepInfo + "Setear el campo '" & locator & "' con el valor '" & value(0) & "'"
		stepInfo = stepInfo + "|El campo se ha compleado con éxito"
		If status = 0 Then
			stepInfo = stepInfo + "|El campo se ha compleado con éxito"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|El campo no se ha compleado con éxito"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecuta"
		End if
	Case "Select"
		stepInfo = stepInfo + "Select|"
		stepInfo = stepInfo + "Seleccionar el combo '" & locator & "' con el valor '" & value(0) & "'"
		stepInfo = stepInfo + "|El combo se ha seleccionado con éxito"
		If status = 0 Then
			stepInfo = stepInfo + "|El combo se ha seleccionado con éxito"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|El combo no se ha seleccionado con éxito"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecuta"
		End if
    Case "SendKey"
        stepInfo = stepInfo + "SendKey|"
        stepInfo = stepInfo + "Presionar en el campo '" & locator & "' la(s) tecla(s) '" & value(0) & "'"
        stepInfo = stepInfo + "|La tecla es presionada con éxito"
        If status = 0 Then
            stepInfo = stepInfo + "|La tecla es presionada con éxito"
        ElseIf status = 1 Then
            stepInfo = stepInfo + "|La tecla no es presionada con éxito"
        Else
            stepInfo = stepInfo + "|La acción no puede ser ejecuta"
        End if
	
    Case "Sync"
       stepInfo = stepInfo + "Sync|"
        stepInfo = stepInfo + "Sincronizar el campo '" & locator & "' con el valor '" & value(0) & "'"
        stepInfo = stepInfo + "|El campo esta sincronizada con éxito"
        If status = 0 Then
            stepInfo = stepInfo + "|El campo esta sincronizada con éxito"
        ElseIf status = 1 Then
            stepInfo = stepInfo + "|El campo no esta sincronizada con éxito"
        Else
            stepInfo = stepInfo + "|La acción no puede ser ejecuta"
       End if
    Case "SetCellData"
       stepInfo = stepInfo + "SetCellData|"
        stepInfo = stepInfo + "Setear la celda '" & locator & "' en la posición '" & value(0) & "," & value(1) & "' con el valor '" & value(2) & "'"
        stepInfo = stepInfo + "|El valor se inserta con éxito"
        If status = 0 Then
            stepInfo = stepInfo + "|El valor se inserta con éxito"
        ElseIf status = 1 Then
            stepInfo = stepInfo + "|El valor no se inserta con éxito"
        Else
            stepInfo = stepInfo + "|La acción no puede ser ejecuta"
       End if
    Case "GetCellData"
       stepInfo = stepInfo + "GetCellData|"
        stepInfo = stepInfo + "Obtener el valor de la celda '" & locator & "' en la posición '" & value(0) & "," & value(1) & "'"
        stepInfo = stepInfo + "|El valor se obtuvo con éxito"
        If status = 0 Then
            stepInfo = stepInfo + "|El valor se obtuvo con éxito"
        ElseIf status = 1 Then
            stepInfo = stepInfo + "|El valor no se obtuvo con éxito"
        Else
            stepInfo = stepInfo + "|La acción no puede ser ejecuta"
       End if
    Case "SelectCell"
       stepInfo = stepInfo + "SelectCell|"
        stepInfo = stepInfo + "Seleccionar la celda '" & locator & "' en la posición '" & value(0) & "," & value(1) & "'"
        stepInfo = stepInfo + "|La celda se ha seleccionado con éxito"
        If status = 0 Then
            stepInfo = stepInfo + "|La celda se ha seleccionado con éxito"
        ElseIf status = 1 Then
            stepInfo = stepInfo + "|La celda no se ha seleccionado con éxito"
        Else
            stepInfo = stepInfo + "|La acción no puede ser ejecuta"
       End if
	 Case "ClickCell"
       stepInfo = stepInfo + "ClickCell|"
        stepInfo = stepInfo + "Clicar en la celda '" & locator & "' en la posición '" & value(0) & "," & value(1) & "'"
        stepInfo = stepInfo + "|La celda se ha seleccionado con éxito"
        If status = 0 Then
            stepInfo = stepInfo + "|La celda se ha seleccionado con éxito"
        ElseIf status = 1 Then
            stepInfo = stepInfo + "La celda no se ha seleccionado con éxito"
        Else
            stepInfo = stepInfo + "|La acción no puede ser ejecuta"
       End if

    Case "ActivateCell"
       stepInfo = stepInfo + "ActivateCell|"
        stepInfo = stepInfo + "Activar la celda '" & locator & "' en la posición '" & value(0) & "," & value(1) & "'"
        stepInfo = stepInfo + "|La celda se ha activado con éxito"
        If status = 0 Then
            stepInfo = stepInfo + "|La celda se ha activado con éxito"
        ElseIf status = 1 Then
            stepInfo = stepInfo + "|La celda no se ha activado con éxito"
        Else
            stepInfo = stepInfo + "|La acción no puede ser ejecuta"
       End if
	Case "SelectRow"
		stepInfo = stepInfo + "SelectRow|"
		stepInfo = stepInfo + "Seleccionar la fila '" & value(0) & "' en la tabla '" & locator & "'"
		stepInfo = stepInfo + "|La fila se ha seleccionado con éxito"
		If status = 0 Then
			stepInfo = stepInfo + "|La fila se ha seleccionado con éxito"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|La fila no se ha seleccionado con éxito"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecuta"
		End if
		
	Case "SelectColumn"
		stepInfo = stepInfo + "SelectColumn|"
		stepInfo = stepInfo + "Seleccionar la columna '" & value(0) & "' en la tabla '" & locator & "'"
		stepInfo = stepInfo + "|La columna se ha seleccionado con éxito"
		If status = 0 Then
			stepInfo = stepInfo + "|La columna se ha seleccionado con éxito"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|La columna no se ha seleccionado con éxito"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecuta"
		End if
	Case "PressButton"
		stepInfo = stepInfo + "PressButton|"
		stepInfo = stepInfo + "Presionar el boton '" & locator & "' en la tabla '" & value(0) & "'"
		stepInfo = stepInfo + "|El botón es presionado con éxito"
		If status = 0 Then
			stepInfo = stepInfo + "|El botón es presionado con éxito"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|El botón no es presionado con éxito"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecuta"
		End if
		
	'================================================================
	'   MOBILE
	'================================================================
	
	Case "Tap"
		stepInfo = stepInfo + "Tap|"
		stepInfo = stepInfo + "Pressionar no item '" & locator & "' en la posición '" & value(0) & "," & value(1) & "'"
		stepInfo = stepInfo + "|O item é pressionado com sucesso"
		If status = 0 Then
			stepInfo = stepInfo + "|O item é pressionado com sucesso"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|O item não é pressionado com sucesso"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecuta"
		End if
	Case "Push"
		stepInfo = stepInfo + "Clicar|"
		stepInfo = stepInfo + "Clicar no item '" & locator & "'"
		stepInfo = stepInfo + "|O item é clicado com sucesso"
		If status = 0 Then
			stepInfo = stepInfo + "|O item é clicado com sucesso"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|O item não foi clicado com sucesso"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecuta"
		End if
		Case "Back"
		stepInfo = stepInfo + "Back|"
		stepInfo = stepInfo + "Voltar a tela anterior anterior do '" & locator & "'"
		stepInfo = stepInfo + "|A tela é retornada com sucesso"
		If status = 0 Then
			stepInfo = stepInfo + "|A tela é retornada com sucesso"
		ElseIf status = 1 Then
			stepInfo = stepInfo + "|A tela não é retornada com sucesso"
		Else
			stepInfo = stepInfo + "|La acción no puede ser ejecuta"
		End if
    Case "TapMove"
       stepInfo = stepInfo + "TapMove|"
        stepInfo = stepInfo + "Arrastar a tela do celular '" & locator & "' da posição " & value(0) & ", " & value(1) & " para a posição " & value(2) & ", " & value(3) 
        stepInfo = stepInfo + "|A tela é arrastada com sucesso"
        If status = 0 Then
            stepInfo = stepInfo + "|A tela é arrastada com sucesso"
        ElseIf status = 1 Then
            stepInfo = stepInfo + "|A tela não é arrastada com sucesso"
        Else
            stepInfo = stepInfo + "|La acción no puede ser ejecuta"
   End if		
		
    Case "Drag"
       stepInfo = stepInfo + "Drag|"
        stepInfo = stepInfo + "Arrastar a tela do celular '" & locator & "' por " & value(0) & " ms, da posição " & value(1) & ", " & value(2) & " para a posição " & value(3) & ", " & value(4) 
        stepInfo = stepInfo + "|A tela é arrastada com sucesso"
        If status = 0 Then
            stepInfo = stepInfo + "|A tela é arrastada com sucesso"
        ElseIf status = 1 Then
            stepInfo = stepInfo + "|A tela não é arrastada com sucesso"
        Else
            stepInfo = stepInfo + "|La acción no puede ser ejecuta"
   		End if
	'Case "Click"
	'Case "Click"
	Case Else
		stepInfo = stepInfo + action + "|"
        stepInfo = stepInfo + "Acción no mapeada :(" 
        stepInfo = stepInfo + "|Acción realizada con éxito"
        If status = 0 Then
            stepInfo = stepInfo + "|Acción realizada con éxito"
        ElseIf status = 1 Then
            stepInfo = stepInfo + "|Acción no realizada con éxito"
        Else
            stepInfo = stepInfo + "|La acción no puede ser ejecuta"
   		End if
	End Select
	stepInfo = stepInfo + "|" + timeExec
	If evidenceDepth > 0 then
		stepInfo = stepInfo + efaTakeEvidenceWithStep(action, status)
	else
		stepInfo = stepInfo + "|"	
	end if
	If status = 1 Then stringOnError = stepInfo
	actionList.Add stepInfo
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' efaTakeEvidence - Use manually for take evidence
' action as string
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Sub efaTakeEvidence(action)
	Dim ScreenName
	
	
	testName = databaseGetValue(testCaseRow, 1)
  On Error Resume Next
  Dim m
  Dim d
  Dim nowHour
  Dim nowMinute
  Dim nowSecond
  
  If Month(Now) < 10 Then
  		m = "0"
  	Else
  		m = ""
  End If
  If Day(Now) < 10 Then
  		d = "0"
  	Else
  		d = ""
  End If
  
  nowHour = Mid(Time, 1, 2)
  nowMinute = Mid(Time, 4, 2)
  nowSecond = Mid(Time, 7, 2)
  
  CurrentTime = Year(Now) & "_" & m & Month(Now) & "_" & d & Day(Now) & "_" & nowHour & "_" & nowMinute & "_" & nowSecond
	
	If Not oFSO.FolderExists(pathEvidence & "\" & testName) Then
	  oFSO.CreateFolder pathEvidence & "\" & testName
	End If
  ScreenName = pathEvidence & "\" & testName & "\" & testName & "_" & getScene & "_" & getStep & "_" & action & "_" & CurrentTime & ".png"
  testStep = testStep + 1
  'ScreenName ="C:\Natura\Evidence"&"\"&ScreenShotName
  Desktop.CaptureBitmap ScreenName,True
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' efaTakeEvidenceWithStep - For each step exist the classification of status and Take evidence of each command (sett, click, clickcell and etc..)
' action as string
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function efaTakeEvidenceWithStep(action, status)
	If status = 4 Then
		efaTakeEvidenceWithStep = "|" & "N/A"
		Exit Function
	End If
	Dim ScreenName
	testName = databaseGetValue(testCaseRow, 1)
  On Error Resume Next
  Dim m
  Dim d
  If Month(Now) < 10 Then
  		m = "0"
  	Else
  		m = ""
  End If
  If Day(Now) < 10 Then
  		d = "0"
  	Else
  		d = ""
  End If
	If Not oFSO.FolderExists(pathEvidence & "\" & testName) Then
		oFSO.CreateFolder pathEvidence
	  oFSO.CreateFolder pathEvidence & "\" & testName
	End If
	
  nowHour = Mid(Time, 1, 2)
  nowMinute = Mid(Time, 4, 2)
  nowSecond = Mid(Time, 7, 2)
  
  CurrentTime = Year(Now) & "_" & m & Month(Now) & "_" & d & Day(Now) & "_" & nowHour & "_" & nowMinute & "_" & nowSecond
	ScreenName = pathEvidence & "\" & testName & "\" & testName & "_" & getScene & "_" & getStep & "_" & action & "_" & CurrentTime & ".png"
	testStep = testStep + 1
	'ScreenName ="C:\Natura\Evidence"&"\"&ScreenShotName
	If applicationNameEvidence = "" Then
		Desktop.CaptureBitmap ScreenName,True
		Else
		Execute(applicationAppEvidence & "(""" & applicationNameEvidence & """).CaptureBitmap """ & ScreenName & """")
		'MDevice("MDevice").CaptureBitmap
	End If
	
	efaTakeEvidenceWithStep = "|" & ScreenName
End Function

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' efaTakeEvidence - Use manually for take evidence
' action as string
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 

Public Sub captureImageTerminal(objScreen)
	testName = databaseGetValue(testCaseRow, 1)	
	Dim ScreenName
	  On Error Resume Next
	  Dim m
	  Dim d
	  Dim nowHour
	  Dim nowMinute
	  Dim nowSecond
	  
	  If Month(Now) < 10 Then
	  		m = "0"
	  	Else
	  		m = ""
	  End If
	  If Day(Now) < 10 Then
	  		d = "0"
	  	Else
	  		d = ""
	  End If
	  
	  tiempo = Split(Time,":")
	  
	  If len(tiempo(0))=1 Then
	  	nowHour = "0"& tiempo(0)
	  else
	  	nowHour = tiempo(0)
	  End If
	  
	  nowMinute = tiempo(1)
	  splitSecond = Split(tiempo(0)," ")
	  nowSecond = splitSecond(0)
	  
    CurrentTime = Year(Now) & m & Month(Now) & d & Day(Now) &"_"& nowHour & nowMinute & nowSecond
	
	If not pathStorageEvidence = testName &"_"& testCaseRow Then
		pathStorageEvidence = testName &"_"& testCaseRow 
		pathImages = pathEvidence & CurrentTime & "\" & testName &"_"& testCaseRow &"\"
		crearArbol( pathImages )
		
	End If	
		
	ScreenName = pathEvidence & CurrentTime & "\" & testName &"_"& testCaseRow & "\" & getStep & ".png"
  	testStep = testStep + 1
  	objScreen.CaptureBitmap ScreenName,True
    'objScreen.CaptureBitmap(ScreenName)
		
	
End Sub

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' contarArchivos - Obtiene la cantindad de archivos en una ruta indicada
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Sub fileCount ()

Dim objFolder, objFile
Set objFolder = oFSO.GetFolder(pathImages) 

For Each objFile In objFolder.Files  
	if LCase((oFSO.GetExtensionName(objFile))) = LCase("png") then  
		countImages = countImages + 1  
		arrList.Add objFile.name
	end if  
Next 
	
End Sub

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' Sub saveEvidenceWord - Save all evidence in a word
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
Sub saveEvidenceWord(rutaPlantillaEvidence) 
    
 Dim wrd
   	SystemUtil.CloseProcessByName("WINWORD.EXE")
    Set wrd = GetObject("","Word.Application") 
	wrd.Visible = True 
	wrd.Documents.Open rutaPlantillaEvidence & "TemplateEvidencia.docx" 
	
	Set objSelection = wrd.Selection
	
	If wrd.ActiveDocument.Bookmarks.Exists("parrafo") = True Then 
 	wrd.ActiveDocument.Bookmarks("parrafo").Select
 	wrd.ActiveDocument.ActiveWindow.Panes(1).Selection.Copy  
 	Set arrList = createObject ("System.Collection.ArrayLyst")
 	fileCount ()
 		
	Set objShape = wrd.ActiveDocument.Shapes
	numPane = 2	
	
	For Iterator = 0 To (arrList.count -1) Step 1
		
		objSelection.InlineShapes.AddPicture pathImages & Cstr(arrList(Iterator))
		objSelection.InsertAfter pathImages & Cstr(arrList(Iterator))
		objSelection.InsertParagraphAfter
		
	Next
	
 
	End If
	countImages = 0
	wrd.ActiveDocument.SaveAs (pathImages & "Evidence.docx")
	wrd.ActiveDocument.Close
	wrd.Quit
	SystemUtil.CloseProcessByName("WINWORD.EXE")
    Set wrd = Nothing
   
    
End Sub

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' writeResults - White results of each case test
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Sub writeResults
	oldTable = oTable
	caseTestName = oldTable & "|" & databaseGetValue(testCaseRow, 1)
	'databaseChangeTable("CasosDeTeste")
	'actionList
	'stepList
	reportRow = getReportRow(caseTestName)
	For i = 1 To actionList.Count
		writeStepToDB reportRow, actionList(i - 1), i, caseTestName, stepList(i - 1)
		reportRow = reportRow + 1
	Next
	databaseChangeTable(oldTable)
	databaseSaveDatabase
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' writeStepToDB - Write description of each step 
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Sub writeStepToDB(row, actionstep, i, oldTable, stepAtribute)
	actionListSplit = Split(actionstep, "|")
	atributeListSplit = Split(stepAtribute, "|")
	If i = 1 Then
		databaseWriteOutput row, 1, oldTable
	End If
	If i <> 1 Then
		If databaseGetValue(row, 2) = "Step 001" Then
			databaseAddRow row
		End If
	End If
	
	databaseWriteOutput row, 2, "Step " & convertToStep(i)
	databaseWriteOutput row, 3, actionListSplit(1)
	databaseWriteOutput row, 4, actionListSplit(2)
	databaseWriteOutput row, 5, actionListSplit(3)
	databaseWriteOutput row, 6, getStatus(atributeListSplit(0))
	databaseWriteOutput row, 7, actionListSplit(UBound(actionListSplit) - 1)
	databaseWriteOutput row, 8, actionListSplit(UBound(actionListSplit))
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' clearResults - Clean values of variables
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Sub clearResults(SAPTest)
	if SAPTest = False then 
		setOnError(False)
		stringOnError = ""
	End if
	actionList.Clear
	stepList.Clear
	setStep 1
End Sub
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' getReportRow - Search the case test row 
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function getReportRow(oldTable)
	row = 2
	While databaseGetValue(row, 2) <> "" 
		If databaseGetValue(row, 1) = oldTable Then
			getReportRow = row
			Exit Function
		End If
		row = row + 1
	Wend
	getReportRow = row
End Function

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' -------------------- Auxiliar Functions ------------------------------------------
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='  

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' KillProcess - Assistant of Sub TurnDownTests with string, passing the value for finalize (kill) 
' element as string
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Sub KillProcess(element) 
	 Const strComputer = "." 
	  Dim objWMIService, colProcessList
	  Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
	  Set colProcessList = objWMIService.ExecQuery("SELECT * FROM Win32_Process WHERE Name = '"& element &".exe'")
	  For Each objProcess in colProcessList 
	    objProcess.Terminate() 
	  Next  
End Sub


'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' hasTests - verify if exist test in spreadsheet(.xlsx)
' row as Integer
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function hasTests(row)
	If databaseGetValue(row, 3) <> "" Then
		hasTests = true
	else
		hasTests = False
	End If
End Function

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' CheckValue - Valide the end of each case test with information of spreadsheet(.xlsx)
' value as String
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 

Public Sub CheckValue(value)
	databaseChangeTable "TestCase"
	expectedResult = databaseGetValue(testCaseRow, 8)
	if stringOnError <> "" Then
		errorSplit = Split(stringOnError, "|")
		databaseWriteOutput testCaseRow, 7, errorSplit(1) & "|" & errorSplit(3)
		databaseWriteOutput testCaseRow, 6, "Failed"
		Exit Sub
	End if
	If expectedResult = value Then
			databaseWriteOutput testCaseRow, 6, "Passed"
		else
			databaseWriteOutput testCaseRow, 6, "Failed"
	End If
	databaseWriteOutput testCaseRow, 7, value
End Sub


'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' TurnDownTests - Kill all process in name even process
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Sub TurnDownTests
	KillProcess("Excel")
End Sub

'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' -------------------- TIME Functions ------------------------------------------
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' TimeBegin - Time Begin of case test
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 

Public Function TimeBegin
     libTimeSave = Time
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' TimeEnd - Time End of case test
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function TimeEnd
    oldHour = Mid(libTimeSave, 1, 2)
    oldMinute = Mid(libTimeSave, 4, 2)
    oldSecond = Mid(libTimeSave, 7, 2)	
    nowHour = Mid(Time, 1, 2)
    nowMinute = Mid(Time, 4, 2)
    nowSecond = Mid(Time, 7, 2)
    nowSecond = nowSecond - oldSecond
    nowMinute = nowMinute - oldMinute
    nowHour = nowHour - oldHour
    If nowSecond < 0 Then
    	nowSecond = nowSecond + 60
    	nowMinute = nowMinute - 1
    End If
    If nowMinute < 0 Then
    	nowMinute = nowMinute + 60
    	nowHour = nowHour - 1
    End If
    If nowHour < 0 Then nowHour = nowHour + 24
    If nowHour < 10 Then nowHour = "0" & nowHour
    If nowMinute < 10 Then nowMinute = "0" & nowMinute
    If nowSecond < 10 Then nowSecond = "0" & nowSecond
    TimeEnd = nowHour & ":" & nowMinute & ":" & nowSecond
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' TimeBeginTransaction - Time Begin of each transaction
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function TimeBeginTransaction
     libTimeTransaction = Time
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' TimeEndTransaction - Time End of each transaction
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function TimeEndTransaction
    oldHour = Mid(libTimeTransaction, 1, 2)
    oldMinute = Mid(libTimeTransaction, 4, 2)
    oldSecond = Mid(libTimeTransaction, 7, 2)	
    nowHour = Mid(Time, 1, 2)
    nowMinute = Mid(Time, 4, 2)
    nowSecond = Mid(Time, 7, 2)
    nowSecond = nowSecond - oldSecond
    nowMinute = nowMinute - oldMinute
    nowHour = nowHour - oldHour
    If nowSecond < 0 Then
    	nowSecond = nowSecond + 60
    	nowMinute = nowMinute - 1
    End If
    If nowMinute < 0 Then
    	nowMinute = nowMinute + 60
    	nowHour = nowHour - 1
    End If
    If nowHour < 0 Then nowHour = nowHour + 24
    If nowHour < 10 Then nowHour = "0" & nowHour
    If nowMinute < 10 Then nowMinute = "0" & nowMinute
    If nowSecond < 10 Then nowSecond = "0" & nowSecond
    TimeEndTransaction = nowHour & ":" & nowMinute & ":" & nowSecond
End Function
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+='
' DateNow - Get date today
'=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=' 
Public Function DateNow
   theday = Day(Date)
   theMonth = Month(Date)
   theyear = Year(Date)
   DateNow = theday & "." & theMonth & "." & theyear
End Function


Public Sub efaLibrarySetApplicationName(app, name)
	applicationAppEvidence = app
	applicationNameEvidence = name
End Sub



