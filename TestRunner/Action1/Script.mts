﻿Option Explicit
print "*******************************************"
print "TEST START"
print "*******************************************"

Dim basePath, filePath, libPath, tag
Dim hostName, os, systemUser, toolName

Dim x : x = Instr(Environment.Value("TestDir"),"TestRunner")-2 
basePath = mid(Environment.Value("TestDir"),1,x) & "\"

'load libraries
filePath = basePath + "Resources\"
libPath = filePath + "Lib\"
LoadFunctionLibrary libPath + "DataLibrary_v2.vbs", libPath + "ReportUtil.vbs", filePath + "Clases\ApplicationUtils.vbs", libPath + "UftLibraries_v2.vbs", libPath + "UftLibrary_v2.vbs", libPath + "TestcasesLibrary.vbs"


'*****************************************************************
'*****************************************************************
'BEFORE TEST CASES
'obtener variables del sistema para el reporte
hostName = Environment.Value("LocalHostName")
os = Environment.Value("OS")
systemUser = Environment.Value("UserName")
toolName = "UFT"

'crear reporte y asignar datos
createHtmlReport basePath + "Reporte\ReportePrincipal\Extent.html"
setEnvironment hostName, os,toolName, "QA"
setTester systemUser

Reporter.Filter = rfEnableAll'rfDisableAll, rfEnableAll
KillProcess("Excel")
closeBrowsers()

'****************************************************************
'****************************************************************
'RUN TEST CASES
setBasePath basePath
setApplicationURL("https://opensource-demo.orangehrmlive.com/")
tag = Environment.Value("tag")
'If tag = "Funcional" Then
'    databaseSetDatabase "Excel", basePath & "Data\automationv2.xlsx", "Scenario"
'    runTestScenario basePath
'
'    else If tag = "Regresion" Then
'    databaseSetDatabase "Excel", basePath & "Data\testDataReg.xlsx", "Scenario"
'    runTestScenario basePath
'
'    else
'    print "no se encontro tag " & tag
'End If
If tag= "Funcional" Then
	databaseSetDatabase "Excel", basePath & "Data\automationv2.xlsx", "Scenario"
    runTestScenario basePath
End If

'*************************************************************
'*************************************************************
'AFTER TEST CASES
generateReport()
print "Generando reporte de ejecución ...."
TurnDownTests

print "*******************************************"
print "END TEST"
print "*******************************************"
